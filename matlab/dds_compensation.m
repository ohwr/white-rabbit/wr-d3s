fs = 125e6;   % Input sampling frequency.
fpass = 0.02e6; % Frequency band of interest.
m = 1024;  % Decimation factor.
hcic = design(fdesign.interpolator(m,'cic',1,fpass,90,fs));

hcic

gain(hcic, 6)

g = 1/ (2^70)
hd = cascade(dfilt.scalar(g),hcic);

comp = fdesign.ciccomp(hcic.differentialdelay, ...
            hcic.numberofsections,fpass,fpass * 1.6,.1,90,fs/m)

hd(2) = design(comp,'fir')

fvtool(hd(1),hd(2),...
cascade(hd(2),hd(1)),'Fs',[fs fs/m fs])

%fvtool(hd)