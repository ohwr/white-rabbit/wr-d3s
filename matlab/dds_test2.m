lut_size=4096;
dac_bits=12;
lut_bits=18;
dac_bias = 2^(dac_bits-1);
dac_ampl = 2^(dac_bits-1) - 1;
lut_ampl = 2^(lut_bits-1) - 1;
acc_frac=32;

n_sub_taps = 512;
    
lut_real=cos(linspace(0, 2*pi * lut_size/(lut_size + 1), lut_size)) .* lut_ampl;
lut = round(lut_real);

for i = 1:lut_size
    
    
    ni = i+1;
    if(ni > lut_size)
        ni=1;
    end

    y0 = lut(i);
    y1 = lut(ni);
    
    
    slope(i)  = (y1-y0)/(2^acc_frac);
    
    

end

disp('maxs');
disp(max(slope));




fs=500e6;
fout=40.079e6;

tune_res = 0.01;

acc_bits=acc_frac + log2(lut_size);
acc_max = 2^acc_bits;
acc = 0;

nsamples = 32768;
y=zeros(nsamples,1);
y2=zeros(nsamples,1);
delta = round((lut_size * fout/fs) * (2^acc_frac))

disp('ls')
lut_size

for i =1:nsamples
    
    idx = floor(acc / (2^acc_frac)) + 1;
    fb = floor(mod(acc, 2^acc_frac));
    
    
    if(idx > lut_size)
        idx = idx - lut_size;
    end
    
        yt=round((lut(idx) + floor(slope(idx) * fb)) / 2^(lut_bits-dac_bits)); 
    
        
        y(i)= yt; % + randi([-1, 1],1,1);
        y2(i) = round(cos(2*pi*(acc/(2^acc_frac))) * dac_ampl);
    
    acc = acc + delta;
    if (acc >= acc_max) 
        acc = acc - acc_max;
    end
end


%y=y+randi([-1,1],nsamples,1)
f = 20*log10(abs(fft(y.*hanning(nsamples))));
f2 = 20*log10(abs(fft(y2.*hanning(nsamples))));

plot(f - max(f));
hold on;
plot(f2 - max(f2),'r-');
hold off;
grid on;
length(y)
length(hanning(nsamples))

length(y)





